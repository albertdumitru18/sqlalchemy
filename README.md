 # SQLAlchemy


SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL.

SQLAlchemy considers the database to be a relational algebra engine, not just a collection of tables. Rows can be selected from not only tables but also joins and other select statements; any of these units can be composed into a larger structure. SQLAlchemy's expression language builds on this concept from its core.

SQLAlchemy is most famous for its object-relational mapper (ORM), an optional component that provides the data mapper pattern, where classes can be mapped to the database in open ended, multiple ways - allowing the object model and database schema to develop in a cleanly decoupled way from the beginning.

# Everything is an object

There are some steps that you have to make in order to use this mapper but it is not complicated.

Let me show you!

# Create tables

We need a table to work on so let's make one.

## Importing modules 

```python
# Importing sessionmaker, base, and engine

from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, String, Column, Integer
```

## Create engine, session and declare base

```python
# Create engine

# psycopg2 is a postgres adapter in python(://username:password@adress:port/databasename)

engine = create_engine('potgresql+psycopg2://dumi:master@172.25.48.1:5432/localPostgresDB')

# Create session

Session = sessionmaker(bind=engine)
session = Session()

# Declare base

Base = declarative_base()
```

## Create table

```python
# Make a class with table properties

class Player(Base)
    __tablename__='player'

    id = Column(Integer, primary_key=True)
    username = Column(String(50), nullable=False)
    most_played_game = Column(String(50), nullable=False)
    total_hours = Column(Integer)
```

## Migrate data to database

```python
Base.metadata.create_all(engine)
```

## Adding data into table


```python
# Creating player1 

player1 = Player(username='kingslayer', most_played_game='Grand Theft Auto V', total_hours=258)

# Adding data from player1 to the session

session.add(player1)

# Commiting data to database

session.commit()

```

## Get data from table

```python 
# Get all data - to get all data you need to loo trough them

players = session.querry(Player)

for player in players:
    print(player)

# Order data 

players = session.querry(Player).order_by(Player.total_hours)

for player in players:
    print(player)

# Filter data

player = session.querry(Player).filter(Player.username='kingslayer') # if you have multiple entries wse a loop

print(player)
```

## Update data from table

```python
# Get the record

player = session.querry(Player).filter(Player.most_played_game=='Grand Theft Auto V')

# Make changes

player.most_played_game = 'World of Warcraft'

# Commit changes

session.commit()
```

## Delete data form table

```python
# Get the record

player = session.querry(Player).filter(Player.most_played_game=='Grand Theft Auto V')

# Delete the record

session.delete(player)

# Commit changes

session.commit()
```

## Delete table

```python
Player.drop(engine)
```

## Another methods

Another methods to do basic tasks in SQL using SQLAlchemy is by executing the engine directly with the given command:

```python
### Example

variable = engine.execute('DROP TABLE test;') # You can write any SQL command between '' and will be executed.
```




























